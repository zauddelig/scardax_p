from django.shortcuts import render


def home(request):
    t = 'home.html'
    if request.is_ajax():
        t='home_ajax.html'
    return render(request, t)
