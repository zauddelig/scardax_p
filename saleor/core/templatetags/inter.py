from django import template
from babel.numbers import get_currency_symbol
register = template.Library()

@register.filter
def get_symbol(value):
    return get_currency_symbol(value)
