from django.db import models as m
from .base_products import Product
AUTH_USER_MODEL ='userprofile.User'
from django.core.validators import MaxValueValidator,MinValueValidator 
from decimal import Decimal as D
MIN = 0; MAX = 5
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Voto(m.Model):
    prodotto = m.ForeignKey(Product, db_index=True)
    utente = m.ForeignKey(AUTH_USER_MODEL, db_index=True)
    voto = m.SmallIntegerField(validators=[MinValueValidator(MIN),MaxValueValidator(MAX)])
    opinione = m.TextField(null=True, blank=True, db_index=True)
    data= m.DateTimeField(auto_now_add=True)
    class Meta:
        unique_together=('prodotto','utente')
        app_label = 'product'
        ordering = ['data']
    def __str__(self):
        return str(self.prodotto) +' - '+str(self.voto) +' - '+str(self.utente)
    def calcola_media(self, a, n):
        return (n*a+D(self.voto))/D(n+1), n+1
    def ricalcola_media (self, a, n):
        return n-1 and (a(n)-D(self.voto))/D(n-1) or D(0), n-1
    
    def save(self,*args,**kwargs):
        super(Voto, self).save(*args,**kwargs)
        self.prodotto.voto_medio, self.prodotto.numero_voti = self.calcola_media(self.prodotto.voto_medio, self.prodotto.numero_voti)
        self.prodotto.save()
    def delete(self,*args, **kwargs):
        super(Voto, self).delete(*args,**kwargs)
        self.prodotto.voto_medio, self.prodotto.numero_voti = self.ricalcola_media(self.prodotto.voto_medio, self.prodotto.numero_voti)
        self.prodotto.save()
    def dolce_voto(self):
        for voto in range(MAX):
            yield voto<self.voto