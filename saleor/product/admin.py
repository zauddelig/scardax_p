from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from .models import (ProductImage, BagVariant, Bag, ShirtVariant, Shirt,
                     Category, FixedProductDiscount, Color, Alimento, 
                     VarianteAlimento, AlimentoPeso,VarianteAlimentoPeso, Voto)
from .forms import ShirtAdminForm, ProductVariantInline


class ImageAdminInline(admin.StackedInline):
    model = ProductImage

class VarianteAlimentoInline(admin.StackedInline):
    model = VarianteAlimento
    formset = ProductVariantInline
    
class VarianteAlimentoPesoInline(admin.StackedInline):
    model = VarianteAlimentoPeso
    formset = ProductVariantInline
    
class BagVariantInline(admin.StackedInline):
    model = BagVariant
    formset = ProductVariantInline

class AlimentoAdmin(admin.ModelAdmin):
    inlines = [VarianteAlimentoInline, ImageAdminInline]
class AlimentoPesoAdmin(admin.ModelAdmin):
    inlines = [VarianteAlimentoPesoInline, ImageAdminInline]    
class BagAdmin(admin.ModelAdmin):
    inlines = [BagVariantInline, ImageAdminInline]


class ShirtVariant(admin.StackedInline):
    model = ShirtVariant
    formset = ProductVariantInline


class ShirtAdmin(admin.ModelAdmin):
    form = ShirtAdminForm
    list_display = ['name', 'collection', 'admin_get_price_min',
                    'admin_get_price_max']
    inlines = [ShirtVariant, ImageAdminInline]


class ProductCollectionAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(Bag, BagAdmin)

admin.site.register(Shirt, ShirtAdmin)
admin.site.register(Category, MPTTModelAdmin)
admin.site.register(FixedProductDiscount)
admin.site.register(Color)
admin.site.register(Alimento,AlimentoAdmin)
admin.site.register(AlimentoPeso,AlimentoPesoAdmin)
admin.site.register(Voto)