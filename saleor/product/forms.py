from django import forms
from django.utils.translation import pgettext_lazy
from selectable.forms import AutoCompleteWidget
from decimal import Decimal
from ..cart.forms import AddToCartForm, QuantityField
from .models import Bag, Shirt, ShirtVariant, Alimento, VarianteAlimento, AlimentoPeso
from .lookups import CollectionLookup
from django.utils.translation import pgettext, ugettext
#    --/-- add to cart related forms --/--

class BagForm(AddToCartForm):

    def get_variant(self, clean_data):
        return self.product.variants.get(product__color=self.product.color)


class ShirtForm(AddToCartForm):
    
    size = forms.ChoiceField(choices=ShirtVariant.SIZE_CHOICES,
                             widget=forms.RadioSelect())

    def __init__(self, *args, **kwargs):
        super(ShirtForm, self).__init__(*args, **kwargs)
        available_sizes = [
            (p.size, p.get_size_display()) for p in self.product.variants.all()
        ]
        self.fields['size'].choices = available_sizes

    def get_variant(self, clean_data):
        size = clean_data.get('size')
        return self.product.variants.get(size=size,
                                         product__color=self.product.color)
class AlimentoForm(AddToCartForm):
     varianti = forms.ChoiceField(widget=forms.RadioSelect())
     def __init__(self, *args, **kwargs):
        super(AlimentoForm, self).__init__(*args, **kwargs)
        available_variants = [
            (p.name, (p.name + ' - ' + p.price.currency + ' '+ str(p.price.gross.quantize(Decimal('1.00'))))) for p in self.product.variants.all()
        ]
        self.fields['varianti'].choices = available_variants
        pass
        
     def get_variant(self, clean_data):
        varianti = clean_data.get('varianti')
        return self.product.variants.get(name=varianti)
     pass
 
class AlimentoPesoForm(AlimentoForm):
    quantity = QuantityField(label=pgettext('Form field', 'Peso in etti'))
    pass

#    --/-- Admin related forms --/--

class ShirtAdminForm(forms.ModelForm):
    class Meta:
        model = Shirt
        widgets = {
            'collection': AutoCompleteWidget(CollectionLookup)
        }


class ProductVariantInline(forms.models.BaseInlineFormSet):
    def clean(self):
        count = 0
        for form in self.forms:
            if form.cleaned_data:
                count += 1
        if count < 1:
            raise forms.ValidationError(
                pgettext_lazy('Product admin error',
                              'You have to create at least one variant'))

def get_form_class_for_product(product):
    if isinstance(product, Shirt):
        return ShirtForm
    if isinstance(product, Bag):
        return BagForm
    if  isinstance(product, Alimento):
        return AlimentoForm
    if isinstance(product, AlimentoPeso):
        return AlimentoPesoForm
    raise NotImplementedError
