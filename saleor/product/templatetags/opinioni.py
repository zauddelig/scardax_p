from django import template
from ..models import Voto, Product
from django.template.loader import get_template
register = template.Library()

def opinioni(partenza=0,passi=5,prodotto=None):
    '''
    torna numero "passi" recensioni scritte a partire dal numero partenza
    riguardanti il prodotto o tutti i prodotti
    '''
    qs = Voto.objects.all().filter(opinione__isnull=False)
    if type(prodotto) is Product:
        qs = qs.filter(prodotto=prodotto)
    return {'recensioni': qs[partenza:passi]}


@register.inclusion_tag('opinioni/home.html')
def opinioni_recenti():
    return opinioni(0,5,None)


@register.inclusion_tag('opinioni/slider.html')
def slider(partenza=0, passi=4):
    partenza = int(partenza)
    if partenza <= 0:
        partenza = 0
    passi = passi + partenza
    def func(partenza, passi):
        qs = Product.objects.all()
        count = qs.count()
        last = count <= passi
        if last:
            passi = count
        qs = qs.order_by('-voto_medio')[partenza:passi]
        print qs
        print partenza
        print passi
        return {'prodotti': qs, 'last':last}
    return func(partenza, passi)