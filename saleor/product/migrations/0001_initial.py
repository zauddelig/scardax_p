# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'product_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'children', null=True, to=orm['product.Category'])),
            (u'lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            (u'level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal(u'product', ['Category'])

        # Adding model 'Product'
        db.create_table(u'product_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'products', to=orm['product.Category'])),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('collection', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=100, null=True, blank=True)),
            ('voto_medio', self.gf('django.db.models.fields.DecimalField')(default='0', max_digits=3, decimal_places=2)),
            ('numero_voti', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'product', ['Product'])

        # Adding model 'FixedProductDiscount'
        db.create_table(u'product_fixedproductdiscount', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('discount', self.gf('django_prices.models.PriceField')(currency='EUR', max_digits=12, decimal_places=4)),
        ))
        db.send_create_signal(u'product', ['FixedProductDiscount'])

        # Adding M2M table for field products on 'FixedProductDiscount'
        m2m_table_name = db.shorten_name(u'product_fixedproductdiscount_products')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fixedproductdiscount', models.ForeignKey(orm[u'product.fixedproductdiscount'], null=False)),
            ('product', models.ForeignKey(orm[u'product.product'], null=False))
        ))
        db.create_unique(m2m_table_name, ['fixedproductdiscount_id', 'product_id'])

        # Adding model 'Color'
        db.create_table(u'product_color', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=6)),
        ))
        db.send_create_signal(u'product', ['Color'])

        # Adding model 'Bag'
        db.create_table(u'product_bag', (
            (u'product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['product.Product'], unique=True, primary_key=True)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('length', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('width', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('depth', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('color', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['product.Color'])),
        ))
        db.send_create_signal(u'product', ['Bag'])

        # Adding model 'Shirt'
        db.create_table(u'product_shirt', (
            (u'product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['product.Product'], unique=True, primary_key=True)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('length', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('width', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('depth', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('color', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['product.Color'])),
        ))
        db.send_create_signal(u'product', ['Shirt'])

        # Adding model 'BagVariant'
        db.create_table(u'product_bagvariant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stock', self.gf('django.db.models.fields.DecimalField')(default='1', max_digits=10, decimal_places=4)),
            ('name', self.gf('django.db.models.fields.CharField')(default=u'', max_length=128, blank=True)),
            ('price', self.gf('django_prices.models.PriceField')(currency='EUR', max_digits=12, decimal_places=4)),
            ('sku', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'variants', to=orm['product.Bag'])),
        ))
        db.send_create_signal(u'product', ['BagVariant'])

        # Adding model 'ShirtVariant'
        db.create_table(u'product_shirtvariant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stock', self.gf('django.db.models.fields.DecimalField')(default='1', max_digits=10, decimal_places=4)),
            ('name', self.gf('django.db.models.fields.CharField')(default=u'', max_length=128, blank=True)),
            ('price', self.gf('django_prices.models.PriceField')(currency='EUR', max_digits=12, decimal_places=4)),
            ('sku', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'variants', to=orm['product.Shirt'])),
            ('size', self.gf('django.db.models.fields.CharField')(max_length=3)),
        ))
        db.send_create_signal(u'product', ['ShirtVariant'])

        # Adding model 'Alimento'
        db.create_table(u'product_alimento', (
            (u'product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['product.Product'], unique=True, primary_key=True)),
            ('weight', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('length', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('width', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('depth', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'product', ['Alimento'])

        # Adding model 'VarianteAlimento'
        db.create_table(u'product_variantealimento', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default=u'', max_length=128, blank=True)),
            ('price', self.gf('django_prices.models.PriceField')(currency='EUR', max_digits=12, decimal_places=4)),
            ('sku', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'variants', to=orm['product.Alimento'])),
        ))
        db.send_create_signal(u'product', ['VarianteAlimento'])

        # Adding model 'ProductImage'
        db.create_table(u'product_productimage', (
            (u'image_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['django_images.Image'], unique=True, primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'images', to=orm['product.Product'])),
        ))
        db.send_create_signal(u'product', ['ProductImage'])

        # Adding model 'Voto'
        db.create_table(u'product_voto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('prodotto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['product.Product'])),
            ('utente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['userprofile.User'])),
            ('voto', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('opinione', self.gf('django.db.models.fields.TextField')(db_index=True, null=True, blank=True)),
            ('data', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('product', ['Voto'])

        # Adding unique constraint on 'Voto', fields ['prodotto', 'utente']
        db.create_unique(u'product_voto', ['prodotto_id', 'utente_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Voto', fields ['prodotto', 'utente']
        db.delete_unique(u'product_voto', ['prodotto_id', 'utente_id'])

        # Deleting model 'Category'
        db.delete_table(u'product_category')

        # Deleting model 'Product'
        db.delete_table(u'product_product')

        # Deleting model 'FixedProductDiscount'
        db.delete_table(u'product_fixedproductdiscount')

        # Removing M2M table for field products on 'FixedProductDiscount'
        db.delete_table(db.shorten_name(u'product_fixedproductdiscount_products'))

        # Deleting model 'Color'
        db.delete_table(u'product_color')

        # Deleting model 'Bag'
        db.delete_table(u'product_bag')

        # Deleting model 'Shirt'
        db.delete_table(u'product_shirt')

        # Deleting model 'BagVariant'
        db.delete_table(u'product_bagvariant')

        # Deleting model 'ShirtVariant'
        db.delete_table(u'product_shirtvariant')

        # Deleting model 'Alimento'
        db.delete_table(u'product_alimento')

        # Deleting model 'VarianteAlimento'
        db.delete_table(u'product_variantealimento')

        # Deleting model 'ProductImage'
        db.delete_table(u'product_productimage')

        # Deleting model 'Voto'
        db.delete_table(u'product_voto')


    models = {
        u'django_images.image': {
            'Meta': {'object_name': 'Image'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'product.alimento': {
            'Meta': {'object_name': 'Alimento', '_ormbases': [u'product.Product']},
            'depth': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['product.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'product.bag': {
            'Meta': {'object_name': 'Bag', '_ormbases': [u'product.Product']},
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Color']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['product.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'product.bagvariant': {
            'Meta': {'object_name': 'BagVariant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '128', 'blank': 'True'}),
            'price': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'variants'", 'to': u"orm['product.Bag']"}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'stock': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '10', 'decimal_places': '4'})
        },
        u'product.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'children'", 'null': 'True', 'to': u"orm['product.Category']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'product.color': {
            'Meta': {'object_name': 'Color'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'product.fixedproductdiscount': {
            'Meta': {'object_name': 'FixedProductDiscount'},
            'discount': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['product.Product']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'products'", 'to': u"orm['product.Category']"}),
            'collection': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'numero_voti': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'voto_medio': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '3', 'decimal_places': '2'})
        },
        u'product.productimage': {
            'Meta': {'ordering': "[u'id']", 'object_name': 'ProductImage', '_ormbases': [u'django_images.Image']},
            u'image_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['django_images.Image']", 'unique': 'True', 'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'images'", 'to': u"orm['product.Product']"})
        },
        u'product.shirt': {
            'Meta': {'object_name': 'Shirt', '_ormbases': [u'product.Product']},
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Color']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['product.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'product.shirtvariant': {
            'Meta': {'object_name': 'ShirtVariant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '128', 'blank': 'True'}),
            'price': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'variants'", 'to': u"orm['product.Shirt']"}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'stock': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '10', 'decimal_places': '4'})
        },
        u'product.variantealimento': {
            'Meta': {'object_name': 'VarianteAlimento'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '128', 'blank': 'True'}),
            'price': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'variants'", 'to': u"orm['product.Alimento']"}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'})
        },
        'product.voto': {
            'Meta': {'ordering': "['data']", 'unique_together': "(('prodotto', 'utente'),)", 'object_name': 'Voto'},
            'data': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'opinione': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'prodotto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'utente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['userprofile.User']"}),
            'voto': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        u'userprofile.address': {
            'Meta': {'object_name': 'Address'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'country_area': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'street_address_1': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'street_address_2': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'})
        },
        u'userprofile.addressbook': {
            'Meta': {'unique_together': "((u'user', u'alias'),)", 'object_name': 'AddressBook'},
            'address': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'unique': 'True', 'to': u"orm['userprofile.Address']"}),
            'alias': ('django.db.models.fields.CharField', [], {'default': "u'Home'", 'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'address_book'", 'to': u"orm['userprofile.User']"})
        },
        u'userprofile.user': {
            'Meta': {'object_name': 'User'},
            'addresses': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['userprofile.Address']", 'through': u"orm['userprofile.AddressBook']", 'symmetrical': 'False'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'default_billing_address': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['userprofile.AddressBook']"}),
            'default_shipping_address': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['userprofile.AddressBook']"}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['product']