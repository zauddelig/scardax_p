# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Alimento.weight'
        db.delete_column(u'product_alimento', 'weight')

        # Deleting field 'Alimento.length'
        db.delete_column(u'product_alimento', 'length')

        # Deleting field 'Alimento.width'
        db.delete_column(u'product_alimento', 'width')

        # Deleting field 'Alimento.depth'
        db.delete_column(u'product_alimento', 'depth')

        # Adding field 'VarianteAlimento.weight'
        db.add_column(u'product_variantealimento', 'weight',
                      self.gf('django.db.models.fields.DecimalField')(default=1, max_digits=6, decimal_places=2),
                      keep_default=False)

        # Adding field 'VarianteAlimento.length'
        db.add_column(u'product_variantealimento', 'length',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)

        # Adding field 'VarianteAlimento.width'
        db.add_column(u'product_variantealimento', 'width',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)

        # Adding field 'VarianteAlimento.depth'
        db.add_column(u'product_variantealimento', 'depth',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Alimento.weight'
        db.add_column(u'product_alimento', 'weight',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2),
                      keep_default=False)

        # Adding field 'Alimento.length'
        db.add_column(u'product_alimento', 'length',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)

        # Adding field 'Alimento.width'
        db.add_column(u'product_alimento', 'width',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)

        # Adding field 'Alimento.depth'
        db.add_column(u'product_alimento', 'depth',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)

        # Deleting field 'VarianteAlimento.weight'
        db.delete_column(u'product_variantealimento', 'weight')

        # Deleting field 'VarianteAlimento.length'
        db.delete_column(u'product_variantealimento', 'length')

        # Deleting field 'VarianteAlimento.width'
        db.delete_column(u'product_variantealimento', 'width')

        # Deleting field 'VarianteAlimento.depth'
        db.delete_column(u'product_variantealimento', 'depth')


    models = {
        u'django_images.image': {
            'Meta': {'object_name': 'Image'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'product.alimento': {
            'Meta': {'object_name': 'Alimento', '_ormbases': [u'product.Product']},
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['product.Product']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'product.bag': {
            'Meta': {'object_name': 'Bag', '_ormbases': [u'product.Product']},
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Color']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['product.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'product.bagvariant': {
            'Meta': {'object_name': 'BagVariant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '128', 'blank': 'True'}),
            'price': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'variants'", 'to': u"orm['product.Bag']"}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'stock': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '10', 'decimal_places': '4'})
        },
        u'product.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'children'", 'null': 'True', 'to': u"orm['product.Category']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        u'product.color': {
            'Meta': {'object_name': 'Color'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'product.fixedproductdiscount': {
            'Meta': {'object_name': 'FixedProductDiscount'},
            'discount': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['product.Product']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'products'", 'to': u"orm['product.Category']"}),
            'collection': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'numero_voti': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'voto_medio': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '3', 'decimal_places': '2'})
        },
        u'product.productimage': {
            'Meta': {'ordering': "[u'id']", 'object_name': 'ProductImage', '_ormbases': [u'django_images.Image']},
            u'image_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['django_images.Image']", 'unique': 'True', 'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'images'", 'to': u"orm['product.Product']"})
        },
        u'product.shirt': {
            'Meta': {'object_name': 'Shirt', '_ormbases': [u'product.Product']},
            'color': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Color']"}),
            'depth': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            u'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['product.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'product.shirtvariant': {
            'Meta': {'object_name': 'ShirtVariant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '128', 'blank': 'True'}),
            'price': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'variants'", 'to': u"orm['product.Shirt']"}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'stock': ('django.db.models.fields.DecimalField', [], {'default': "'1'", 'max_digits': '10', 'decimal_places': '4'})
        },
        u'product.variantealimento': {
            'Meta': {'object_name': 'VarianteAlimento'},
            'depth': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '128', 'blank': 'True'}),
            'price': ('django_prices.models.PriceField', [], {'currency': "'EUR'", 'max_digits': '12', 'decimal_places': '4'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'variants'", 'to': u"orm['product.Alimento']"}),
            'sku': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'width': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        'product.voto': {
            'Meta': {'ordering': "['data']", 'unique_together': "(('prodotto', 'utente'),)", 'object_name': 'Voto'},
            'data': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'opinione': ('django.db.models.fields.TextField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'prodotto': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'utente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['userprofile.User']"}),
            'voto': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        u'userprofile.address': {
            'Meta': {'object_name': 'Address'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'country_area': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'street_address_1': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'street_address_2': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'})
        },
        u'userprofile.addressbook': {
            'Meta': {'unique_together': "((u'user', u'alias'),)", 'object_name': 'AddressBook'},
            'address': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'unique': 'True', 'to': u"orm['userprofile.Address']"}),
            'alias': ('django.db.models.fields.CharField', [], {'default': "u'Home'", 'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'address_book'", 'to': u"orm['userprofile.User']"})
        },
        u'userprofile.user': {
            'Meta': {'object_name': 'User'},
            'addresses': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['userprofile.Address']", 'through': u"orm['userprofile.AddressBook']", 'symmetrical': 'False'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'default_billing_address': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['userprofile.AddressBook']"}),
            'default_shipping_address': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['userprofile.AddressBook']"}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['product']