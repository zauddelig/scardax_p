// GPL 3.0 - Fabrizio Ettore Messina zauddelig@gmail.com
 $(function(){
	var UI_common={
            origin: window.location.origin || window.location.protocol+"//"+window.location.host,
            container:$('#magicbox'),
            trigger:function(){
                var _this = this;
                UI_common.container.on('click',_this.anchor,function(ev){
                    ev.preventDefault();
                    UI_common.get($(this).attr('href'));
                });
            
            },
            navigator_trigger:function(){
                $('a.navigator').on('click',function(ev){
                    ev.preventDefault();
                    UI_common.get($(this).attr('href'));
                });
            },
            get:function(url){
                var _this = this,
                c      = _this.container,
                callback = function(data, s, xhr){
                    c.promise().done(function(){
                        c.children().remove();
                        c.append(data);
                        c.fadeIn();
                        c.trigger('url_changed');
                    });
                };
                try{
                    history.pushState([],'',url);//vitale che la storia cambi con le richieste ajax.
                    
                    $.ajax({
                        url:url,
                        success:callback,
                        beforeSend:function(){c.hide('scale');}
                    });
                }catch(err){
                    windows.open(_this.origin +'/'+url);//no html5 fallback
                }
            }
        },
	UI_product={
                regex:new RegExp("/products/\.+/?",'i'),
		price:null,
		quantity:null,
		final_price:$("#final-price")[0],
		qty_input: $('.quantity input'),
		button : $(".form-group button"),
                init_vars:function(){
                    this.final_price=$("#final-price")[0];
                    this.qty_input= $('.quantity input');
                    this.button = $(".form-group button");
                },
		get_quantity:function(){
			var qty=this.qty_input.val(); 
			if(qty<=0){
				this.button.attr("disabled", "disabled");
				qty = 0;
			}else{
				this.button.attr("disabled") && this.button.removeAttr("disabled");
			}
			this.quantity = qty;
			return qty;
		},
		get_price:function(ev){
			if(ev==null){
				return this.price;
			}
			var a = $(ev.target).parent()[0].lastChild.data.trim().split(' ');
                        
			this.price = a[a.length-1] + 0.0;
			return this.price;
		},
		varianti_trigger:function(){
			var _this = this,
			display_price = function(ev){
                                _this.init_vars();
				var price =  ev==null?_this.price:_this.get_price(ev), qty =_this.get_quantity();
				price = (((_this.get_quantity()*price+0.00001)*100)/100 + "").split(".");
				try{
					var p = price[1];
					while(p.length<2){p=p+"0";};
					p = p.substr(0,2);
					price[1]=p;
				}
				catch(err){
					price[1]="00";
				};
				price = price.join('.');
				_this.final_price.innerHTML=price;
			},
			display_qprice= function(){return display_price(null)};
			UI_common.container.on('click', '.varianti input', display_price);
                        UI_common.container.on('click', '.quantity input', display_qprice);
		},
                navigator_trigger:UI_common.navigator_trigger,
		init:function(){
			this.varianti_trigger();
                        this.navigator_trigger.bind(this)();
		}
	},
        UI_category={
            regex:new RegExp("/products/category/",'i'),
            anchor:'.product>a',
            trigger:UI_common.trigger,
            init:function(){
                this.trigger.bind(this)();
            }
        },
        UI_ricette={
            regex:new RegExp("^/ricette/$",'i'),
            anchor:'.product>a',
            trigger:UI_common.trigger,
            init:function(){
                this.trigger.bind(this)();
            }
        },
        UI_ricetta={
            regex:new RegExp("^/ricette/\.+/?",'i'),
            navigator_trigger:UI_common.navigator_trigger,
            rotate:function(deg){
                text= {
                    'transform':'rotate('+deg+'deg)',
                    '-ms-transform':'rotate('+deg+'deg)',
                    '-webkit-transform':'rotate('+deg+'deg)'}
                return text
            },
            clock:function(){
                var clock=$('.orologio'),
                hour = clock.data('hour')*15,
                minute = clock.data('minute')*6;
                $.each(this.rotate(hour),function(k,v){$(clock.children()[0]).css(k,v)});
                $.each(this.rotate(minute),function(k,v){$(clock.children()[1]).css(k,v)});
            },
            init:function(){
                        this.navigator_trigger.bind(this)();
                        this.clock();
                }
        },
        UI_slider={
            regex:new RegExp("^/#?$"),
            url:"/products/slider/",
            counter:0,
            slider:null,
            magic:null,
            get:function(bool){
                var _this = this,
                prev=$(_this.slider.children('a')[0]),
                next=$(_this.slider.children('a')[1]),
                c=_this.magic,
                h=c.height(),
                callback=function(data,s,xhr){
                    c.promise().done(function(){
                        c.height(h);
                        c.children().remove();
                        c.append(data);
                        c.show("drop",'easeInOutCubic');
                        _this.counter>0?prev.removeClass('inactive','1000','linear'):prev.addClass('inactive','1000','linear');
                        _this.magic.children('.last').length>0?next.addClass('inactive','1000','linear'):next.removeClass('inactive','1000','linear');
                    });
                };
                this.counter = bool?_this.counter+4:_this.counter-4;
                if (_this.counter <=0){
                    _this.counter=0;
                }
                $.ajax({
                    url:_this.url+_this.counter,
                    success:callback,
                    beforeSend:function(){c.hide("drop",'easeInOutCubic'); }
                })
            },
            init:function(){
                var _this = this;
                this.counter=0;
                this.slider=$('.slider.row');
                this.magic=$(this.slider.children('div'));
                this.slider.children('a').on('click',function(){
                    var $this = $(this);
                    bool = $this.data('slide')==='next'
                    _this.get(bool);
                });
                UI_category.init();
            }
        },
	UI_navigator={
            header:$('header a[href!=#]'),
            _url:null,
            _ajax_call:function(){
                UI_common.get(this._url)
            },
            header_trigger:function(){
                var _this = this;
                this.header.on('click',function(ev){
                    ev.preventDefault();
                    var $this=$(this);
                    _this._url=$this.attr('href');
                    _this._ajax_call();
                });
            },
            init:function(){
                this.header_trigger();
                $('.dropdown').mouseover(function() {
                    $(this.children[0]).dropdown('toggle')
                })
                .mouseout(function() {
                    $(this.children[0]).dropdown('toggle')
                });
            }
	},
        UI_controller={
            controller_blocking:[
                UI_category,
                UI_product,
                UI_ricette,
                UI_ricetta
            ],
            controller_nonblocking:[
                UI_slider
            ],
            controll:function(url){
                $.each(this.controller_blocking,function(i,el){
                    if(el.regex.test(url)){
                        el.init();
                        return false;
                    }
                });
                $.each(this.controller_nonblocking,function(i,el){
                    if(el.regex.test(url)){
                        el.init();
                    }
                });
            },
            init:function(){
                var _this=this;
                _this.controll(window.location.pathname);
                UI_common.container.on('url_changed',function(){
                    _this.controll(window.location.pathname);
                });
            }
              
        },
	UI = {
            init:function(){
                UI_navigator.init();
                UI_controller.init();
            }
	};
	UI.init();
});

