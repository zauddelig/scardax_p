from django.db import models as m
from django_hstore import hstore as h
from django.utils.encoding import python_2_unicode_compatible
from django_images.models import Image
from django.core.urlresolvers import reverse
class Ricetta(m.Model):
    objects = h.HStoreManager()
    titolo = m.CharField(max_length=120)
    slug = m.SlugField(unique=True)
    url_video= m.URLField(blank=True, null=True)
    descrizione = m.TextField()
    ricetta=m.TextField()
    ingredienti=h.DictionaryField()
    persone = m.SmallIntegerField()
    tempo = m.TimeField()
    aggiunto_il = m.DateTimeField(auto_now_add=True)
    def get_absolute_url(self):
        return reverse('ricette:ricetta',kwargs={'slug':self.slug})
    def __str__(self):
        return self.titolo
    def mostra_tempo(self):
        '''andrebbe in un template tag'''
        ore = self.tempo.hour
        if ore != 0:
            return "%sh:%s'"%(ore, self.tempo.__format__('%m'))
        return "%s min"%self.tempo.minute
        
    class Meta:
        verbose_name_plural = 'ricette'
        ordering = ['-aggiunto_il','titolo']

class ImageManager(m.Manager):
    def first(self):
        return self.get_query_set()[0]
@python_2_unicode_compatible
class ImmagineRicetta(Image):
    ricetta = m.ForeignKey(Ricetta, related_name='images')

    objects = ImageManager()

    class Meta:
        ordering = ['id']
        app_label = 'ricette'

    def __str__(self):
        html = '<img src="%s" alt="">' % (
            self.get_absolute_url('admin'),)
        from django.utils.safestring import mark_safe
        return mark_safe(html)