from django.contrib import admin
from .models import Ricetta, ImmagineRicetta

class ImmagineRicettaAdminInline(admin.StackedInline):
    model = ImmagineRicetta
    
class RicettaAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("titolo",)}
    inlines = [ImmagineRicettaAdminInline]    
admin.site.register(Ricetta,RicettaAdmin)