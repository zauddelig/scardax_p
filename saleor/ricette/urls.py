from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns(
    '',
    url(r'^$',views.Index.as_view(), name='index'),
    url(r'^(?P<slug>[-_\w]+)$',views.DettaglioRicetta.as_view(), name='ricetta'),
)
