from django.views.generic import ListView, DetailView
from .models import Ricetta
class Index(ListView):
    model = Ricetta
    context_object_name = 'ricette'
    template_name = 'ricette/index'
    ajax=False
    def get_template_names(self):
        return [self.template_name + (self.request.is_ajax() and '_ajax' or '') + '.html']
        
        pass
class DettaglioRicetta(DetailView):
    model = Ricetta
    context_object_name = 'ricetta'
    def get_template_names(self):
        return 'ricette/ricetta'+(self.request.is_ajax() and '_ajax' or '')+'.html'
        pass
    pass