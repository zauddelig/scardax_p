from django import template
from ..models import Ricetta
register = template.Library()

@register.inclusion_tag('home/ricetta.html')
def ricetta_home():
    try:
        url = Ricetta.objects.filter(url_video__isnull=False)[0].url
    except:
        url = '//www.youtube.com/embed/UQ0jL4_zxxU'
    return {'url':url}


@register.filter
def mostra_tempo(tempo):
        '''andrebbe in un template tag'''
        ore = tempo.hour
        if ore != 0:
            return "%sh:%s'"%(ore, tempo.__format__('%m'))
        return "%s min"%tempo.minute
    
def ruota_css(deg):
    return 'transform:rotate('+deg+'deg);-ms-transform:rotate('+deg+'deg);-webkit-transform:rotate('+deg+'deg);'

@register.filter
def lancetta_ore(ora):
    deg = ora*15
    return ruota_css(str(deg))

@register.filter
def lancetta_minuti(minuti):
    deg = minuti*6
    return ruota_css(str(deg-180))