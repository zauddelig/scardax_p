from django import template

register = template.Library()

@register.filter
def mostra_tempo(tempo):
        '''andrebbe in un template tag'''
        ore = tempo.hour
        if ore != 0:
            return "%sh:%s'"%(ore, tempo.__format__('%m'))
        return "%s min"%tempo.minute