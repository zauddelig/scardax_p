# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Ricetta'
        db.create_table(u'ricette_ricetta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titolo', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('url_video', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('descrizione', self.gf('django.db.models.fields.TextField')()),
            ('ricetta', self.gf('django.db.models.fields.TextField')()),
            ('ingredienti', self.gf(u'django_hstore.fields.DictionaryField')()),
            ('persone', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('tempo', self.gf('django.db.models.fields.TimeField')()),
        ))
        db.send_create_signal(u'ricette', ['Ricetta'])


    def backwards(self, orm):
        # Deleting model 'Ricetta'
        db.delete_table(u'ricette_ricetta')


    models = {
        u'ricette.ricetta': {
            'Meta': {'object_name': 'Ricetta'},
            'descrizione': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredienti': (u'django_hstore.fields.DictionaryField', [], {}),
            'persone': ('django.db.models.fields.SmallIntegerField', [], {}),
            'ricetta': ('django.db.models.fields.TextField', [], {}),
            'tempo': ('django.db.models.fields.TimeField', [], {}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'url_video': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['ricette']