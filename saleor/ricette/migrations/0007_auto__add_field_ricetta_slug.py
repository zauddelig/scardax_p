# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Ricetta.slug'
        db.add_column(u'ricette_ricetta', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='besciamella', max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Ricetta.slug'
        db.delete_column(u'ricette_ricetta', 'slug')


    models = {
        u'django_images.image': {
            'Meta': {'object_name': 'Image'},
            'height': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        'ricette.immaginericetta': {
            'Meta': {'ordering': "['id']", 'object_name': 'ImmagineRicetta', '_ormbases': [u'django_images.Image']},
            u'image_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['django_images.Image']", 'unique': 'True', 'primary_key': 'True'}),
            'ricetta': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['ricette.Ricetta']"})
        },
        u'ricette.ricetta': {
            'Meta': {'ordering': "['-aggiunto_il', 'titolo']", 'object_name': 'Ricetta'},
            'aggiunto_il': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'descrizione': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredienti': (u'django_hstore.fields.DictionaryField', [], {}),
            'persone': ('django.db.models.fields.SmallIntegerField', [], {}),
            'ricetta': ('django.db.models.fields.TextField', [], {}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tempo': ('django.db.models.fields.TimeField', [], {}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'url_video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['ricette']