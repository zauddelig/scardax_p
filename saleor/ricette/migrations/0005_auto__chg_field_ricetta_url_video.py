# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Ricetta.url_video'
        db.alter_column(u'ricette_ricetta', 'url_video', self.gf('django.db.models.fields.URLField')(max_length=200, null=True))

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Ricetta.url_video'
        raise RuntimeError("Cannot reverse this migration. 'Ricetta.url_video' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'Ricetta.url_video'
        db.alter_column(u'ricette_ricetta', 'url_video', self.gf('django.db.models.fields.URLField')(max_length=200))

    models = {
        u'ricette.ricetta': {
            'Meta': {'ordering': "['-aggiunto_il', 'titolo']", 'object_name': 'Ricetta'},
            'aggiunto_il': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'descrizione': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredienti': (u'django_hstore.fields.DictionaryField', [], {}),
            'persone': ('django.db.models.fields.SmallIntegerField', [], {}),
            'ricetta': ('django.db.models.fields.TextField', [], {}),
            'tempo': ('django.db.models.fields.TimeField', [], {}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'url_video': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['ricette']