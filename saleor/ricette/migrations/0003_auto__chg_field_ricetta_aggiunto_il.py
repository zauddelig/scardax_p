# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Ricetta.aggiunto_il'
        db.alter_column(u'ricette_ricetta', 'aggiunto_il', self.gf('django.db.models.fields.DateTimeField')())

    def backwards(self, orm):

        # Changing field 'Ricetta.aggiunto_il'
        db.alter_column(u'ricette_ricetta', 'aggiunto_il', self.gf('django.db.models.fields.DateField')())

    models = {
        u'ricette.ricetta': {
            'Meta': {'ordering': "['-aggiunto_il', 'titolo']", 'object_name': 'Ricetta'},
            'aggiunto_il': ('django.db.models.fields.DateTimeField', [], {}),
            'descrizione': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingredienti': (u'django_hstore.fields.DictionaryField', [], {}),
            'persone': ('django.db.models.fields.SmallIntegerField', [], {}),
            'ricetta': ('django.db.models.fields.TextField', [], {}),
            'tempo': ('django.db.models.fields.TimeField', [], {}),
            'titolo': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'url_video': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['ricette']